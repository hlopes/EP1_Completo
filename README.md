Para executar o programa, entre na pasta bin e execute arquivo finalBinary.keep, este automaticamente criará na pasta principal do programa:
copia.pgm - copia sem modificações
negativo.pgm - imagem com filtro negativo aplicado
smooth.pgm - imagem com filtro smooth aplicado
sharpen.pgm - imagem com filtro sharpen aplicado

Estando na pasta principal:
1 - Para remover os arquivos .o e o binario
	$make clean

2 - Para compilar o código
	$make
