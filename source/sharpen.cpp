// Henrique Lopes Dutra
// Prof. Paulo Meirelles
// EP1


#include "../include/sharpen.hpp"

#include <iostream>

using namespace std;

Sharpen::Sharpen(){

}


void Sharpen::aplicarFiltro(Imagem &imagem, int div, int size){
    
	int i, j, value, x, y;
	int filtro[] = {0, -1, 0, -1, 5, -1, 0, -1, 0};
	int *aux = new int[imagem.getNumDeLinhas() * imagem.getNumDeColunas()];
    
	for(i=size/2; i<imagem.getNumDeLinhas()-size/2; i++){
        for(j=size/2; j<imagem.getNumDeColunas()-size/2; j++) {
        	
        	value = 0;
        	
        	for(x = -1; x <=1; x++){
        		for(y = -1; y<=1; y++){
        			value += filtro[(x+1)+ size*(y+1)]*imagem.getPixel((i+x), (y+j));
        		}
        	}

        	value /=div;

        	value= value < 0 ? 0 : value;
			value=value >255 ? 255 : value;

			aux[i+imagem.getNumDeColunas()*j] = value;
        }
    }


	for(i=0; i<imagem.getNumDeLinhas(); i++){
        for(j=0; j<imagem.getNumDeColunas(); j++){
        	imagem.modificaPixel(i, j, aux[i+imagem.getNumDeColunas()*j]);
        }
    }

    cout << "Filtro sharpen aplicado com sucesso" << endl;
}


void Sharpen::setDiv(int div){
    
    this->div = div;
}

int Sharpen::getDiv(){
    return div;
}

void Sharpen::setSize(int size){
    this->size = size;
}

int Sharpen::getSize(){
    return size;
}
