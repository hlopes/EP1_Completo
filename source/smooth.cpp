// Henrique Lopes Dutra
// Prof. Paulo Meirelles
// EP1


#include "../include/smooth.hpp"
#include <iostream>
#include <stdlib.h>
#include <vector>

using namespace std;

Smooth::Smooth(){

}

void Smooth::aplicarFiltro(Imagem &imagem){
    
	int i, j, valor=0, x, y;
	int filter[9] = {1, 1, 1, 1, 1, 1, 1, 1, 1};


	for(i=1; i<imagem.getNumDeLinhas()-1; i++){
        for(j=1; j<imagem.getNumDeColunas()-1; j++){
        	
        	valor = 0;

        	valor = (imagem.getPixel((i+1), j) + imagem.getPixel((i+1), (j+1)) + imagem.getPixel((i+1), (j-1)) + imagem.getPixel((i-1), j) + imagem.getPixel((i-1), (j+1)) + imagem.getPixel((i-1), (j-1)) + imagem.getPixel(i, (j+1)) + imagem.getPixel(i, (j-1)))/8;

        	valor = valor < 0 ? 0 : valor;
        	valor = valor > 255 ? 255 : valor;

			imagem.modificaPixel(i, j, (int)valor);
        }
    }

    cout << "Filtro smooth aplicado com sucesso" << endl;
}