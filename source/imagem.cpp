// Henrique Lopes Dutra
// Prof. Paulo Meirelles
// EP1


#include "../include/imagem.hpp"
#include <iostream>
#include <string>

#include <fstream>
#include <sstream>

#include <stdlib.h> 
#include <ctype.h>

using namespace std;

Imagem::Imagem() {
    
    // inicializar todos os atributos como zero ou nulo
    linhas = 0;
    colunas = 0;
    
    tomDeCinza = 0;
    
    matriz = NULL;
}

// implementação dos getters
int Imagem::getNumDeLinhas() {
    return linhas;
}

int Imagem::getNumDeColunas() {
    return colunas;
}

int Imagem::getMaxDeCinza() {
    return tomDeCinza;
}

Imagem::Imagem(int linhasRecebidas, int colunasRecebidas, int cinza) {
    
    this->linhas = linhasRecebidas;
    this->colunas = colunasRecebidas;
    this->tomDeCinza = cinza;
    
    matriz = new int[colunas*linhas];
}
Imagem::~Imagem(){
    delete []matriz;
}

void Imagem::salvarImagem(const char filename[]) {
    int i, j;
    unsigned char *charImage;
    ofstream outfile(filename);
    
    charImage = (unsigned char *) new unsigned char [linhas*colunas];
    
    int val;
    
    for(i=0; i<linhas; i++){
        for(j=0; j<colunas; j++){
            
            val = matriz[i*colunas+j];
            charImage[i*colunas+j]=(unsigned char)val;
        }
    }
    
    if (!outfile.is_open())
    {
        cout << "Falha: Aquivo "  << filename << " não aberto"<< endl;
        exit(1);
    }
    
    outfile << "P5" << endl;
    outfile << numDeLinhas << " " << colunas << endl;
    outfile << 255 << endl;
    
    //pixels
    outfile.write(reinterpret_cast<char *>(charImage), (linhas*colunas)*sizeof(unsigned char));
    
    cout << "Arquivo " << filename << " criado com sucesso!" << endl;
    outfile.close();
}


void Imagem::lerImagem(const char fname[]) {
    int i, j;
    unsigned char *charImage;
    char header [100], *controle;
    ifstream ifp;
    
    ifp.open(fname, ios::in | ios::binary);
    
    if (!ifp)
    {
        cout << "Nao e possivel ler a imagem: " << fname << endl;
        exit(1);
    }
    
    // read header
    
    ifp.getline(header,100,'\n');
    if ( (header[0]!=80) || (header[1]!=53) ) // P5
    {
        cout << "Imagem " << fname << " nao e PGM" << endl;
        exit(1);
    }
    
    ifp.getline(header,100,'\n');
    while(header[0]=='#')
        ifp.getline(header,100,'\n');
    
    linhas=strtol(header,&controle,0);
    
    // atoi pega string e converte para int
    
    colunas=atoi(controle);
    
    
    ifp.getline(header,100,'\n');
    tomDeCinza=strtol(header,&controle,0);
    
    charImage = (unsigned char *) new unsigned char [linhas*colunas];
    
    ifp.read( reinterpret_cast<char *>(charImage), (linhas*colunas)*sizeof(unsigned char));
    
    if (ifp.fail())
    {
        cout << "Imagem " << fname << " esta com o tamanho errado" << endl;
        exit(1);
    }
    
    ifp.close();
    
    int valorr;
    
    for(i=0; i<linhas; i++)
        for(j=0; j<colunas; j++)
        {
            valorr = (int)charImage[i*colunas+j];
            matriz[i*colunas+j] = valorr;
        }
    
    delete [] charImage;
}











void Imagem::modificaPixel(int i, int j, int valor){
    matriz[i*colunas+j] = valor;
}

int Imagem::getPixel(int i, int j){
    return matriz[i*colunas + j];
}