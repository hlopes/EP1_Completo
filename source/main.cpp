// Henrique Lopes Dutra
// Prof. Paulo Meirelles
// EP1


#include "../include/imagem.hpp"
#include "../include/filtro.hpp"
#include "../include/negativo.hpp"
#include "../include/smooth.hpp"
#include "../include/sharpen.hpp"
#include <iostream>

using namespace std;

int main(int argc, char *argv[]){
    
    
	Imagem primeiraImagem(512, 512, 255);

	primeiraImagem.lerImagem("../lena.pgm");
	primeiraImagem.salvarImagem("../copia.pgm");
    
    
    cout << "Olá, bem vindo! Esse é um programa de processamento de imagens. Qual filtro você deseja aplicar?" << endl;
    cout << "1 - Negativo" << "2 - Smooth" << "3 - Sharpen" << endl;
    
    int opcao;
    
    cout << "Opcao: ";
    cin >> opcao;
    
    if(opcao == 1) {
        Negativo filtroNegativo;
        filtroNegativo.aplicarFiltro(primeiraImagem);
        primeiraImagem.salvarImagem("../negativo.pgm");
    }
    
    else if (opcao ==2){
        Imagem segundaImagem(512, 512, 255);
        segundaImagem.lerImagem("../lena.pgm");
        
        Smooth filtroSmooth;
        
        filtroSmooth.aplicarFiltro(segundaImagem);
        segundaImagem.salvarImagem("../smooth.pgm");
    }

	//Aplicando filtro sharpen
    else if(opcao == 3){
        
        Imagem terceiraImagem(512, 512, 255);
        terceiraImagem.lerImagem("../lena.pgm");
        
        Sharpen filtroSharpen;
        
        filtroSharpen.setDiv(1);
        filtroSharpen.setSize(3);
        filtroSharpen.aplicarFiltro(terceiraImagem, filtroSharpen.getDiv(), filtroSharpen.getSize());
        terceiraImagem.salvarImagem("../sharpen.pgm");

    }


	return 0;
}

