// Henrique Lopes Dutra
// Prof. Paulo Meirelles
// EP1


#include "../include/negativo.hpp"
#include "../include/imagem.hpp"

#include <iostream>

using namespace std;

Negativo::Negativo(){

}

void Negativo::aplicarFiltro(Imagem &imagem){
    
	int i, j;

	for(i=0; i<imagem.getNumDeLinhas(); i++)
    {
        for(j=0; j<imagem.getNumDeColunas(); j++) 
        {
        	int novoValor = 255 - imagem.getPixel(i, j);
			imagem.modificaPixel(i, j, novoValor);
        }
    }

    cout << "Filtro negativo feito com sucesso" << endl;
}