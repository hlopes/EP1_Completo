// Henrique Lopes Dutra
// Prof. Paulo Meirelles
// EP1

#ifndef SMOOTH_H
#define SMOOTH_H

#include "filtro.hpp"

class Smooth : public Filtro{

public:
	Smooth();

	void aplicarFiltro(Imagem &imagem);

};

#endif