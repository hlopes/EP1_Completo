// Henrique Lopes Dutra
// Prof. Paulo Meirelles
// EP1


#ifndef _IMAGEM_H_
#define _IMAGEM_H_

using namespace std;

class Imagem
{
	private:
		// Atributos da classe
    
		int linhas;
		int colunas;
    
		int tomDeCinza;
    
		int *matriz;
    
	public:
    
		Imagem();
		Imagem(int linhasRecebidas, int colunasRecebidas, int cinza);
		~Imagem();

		// Gets
		int getNumDeLinhas();
		int getNumDeColunas();
		int getMaxDeCinza();

		void modificaPixel(int i, int j, int valor);
		int getPixel(int i, int j);

		//Métodos de manipulação de imagem, os nomes são auto-explicativos
		void lerImagem(const char fname[]);
		void salvarImagem(const char filename[]);
		
};

#endif	
