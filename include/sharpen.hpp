// Henrique Lopes Dutra
// Prof. Paulo Meirelles
// EP1


#ifndef SHARPEN_H
#define SHARPEN_H

#include "filtro.hpp"

class Sharpen : public Filtro{
private:
	int div;
	int size;
    
public:
	Sharpen();
    
    // set e get
    
	void setDiv(int div);
	int getDiv();
	void setSize(int size);
	int getSize();

    // funcao para aplicativo o sharpen
	void aplicarFiltro(Imagem &imagem, int div, int size);

};

#endif