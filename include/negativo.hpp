// Henrique Lopes Dutra
// Prof. Paulo Meirelles
// EP1


#ifndef NEGATIVO_H
#define NEGATIVO_H

#include "filtro.hpp"


class Negativo : public Filtro{
    
public:
	Negativo();

    //metodo que irá realizar o filtro da imagem (negativo)
	void aplicarFiltro(Imagem &imagem);

};

#endif